<?php

// This file is auto-generated, don't edit it. Thanks.
namespace AlibabaCloud\SDK\Sample;

use AlibabaCloud\SDK\FCOpen\V20210406\FCOpen;
use AlibabaCloud\Tea\Utils\Utils;
use \Exception;
use AlibabaCloud\Tea\Exception\TeaError;

use Darabonba\OpenApi\Models\Config;
use AlibabaCloud\SDK\FCOpen\V20210406\Models\InvokeFunctionHeaders;
use AlibabaCloud\SDK\FCOpen\V20210406\Models\InvokeFunctionRequest;
use AlibabaCloud\Tea\Utils\Utils\RuntimeOptions;

class Sample {

    /**
     * 使用AK&SK初始化账号Client
     * @param string $accessKeyId
     * @param string $accessKeySecret
     * @return FCOpen Client
     */
    public static function createClient($accessKeyId, $accessKeySecret){
        $config = new Config([
            // 必填，您的 AccessKey ID
            "accessKeyId" => $accessKeyId,
            // 必填，您的 AccessKey Secret
            "accessKeySecret" => $accessKeySecret
        ]);
        // 访问的域名 id 看个人信息 账号ID：
        $config->endpoint = "id.cn-hangzhou.fc.aliyuncs.com";
        return new FCOpen($config);
    }

    /**
     * @param string[] $args
     * @return void
     */
    public static function main(){
        // 工程代码泄露可能会导致AccessKey泄露，并威胁账号下所有资源的安全性。以下代码示例仅供参考，建议使用更安全的 STS 方式，更多鉴权访问方式请参见：https://help.aliyun.com/document_detail/311677.html
        $client = self::createClient("xxxx", "xxxxx");
         // Sync：同步调用。 直接返回结果和 回调到url
        // Async：异步调用。  请求地址回调到 url地址
        $invokeFunctionHeaders = new InvokeFunctionHeaders([
            "xFcInvocationType" => "Sync",
            "xFcLogType" => "None"
        ]);
               

        $invokeFunctionRequest = new InvokeFunctionRequest([
                 // 服务的版本ID或者别名。
            "qualifier" => "LATEST",
            // 中文字体写 microhei或者 zenhei
            "body" => Utils::toBytes("{
    \"pdf_file\": \"more.pdf\",  
    \"mark_text\": \"AliyunFC89998\",  
    \"pagesize\": [595.275590551181, 841.8897637795275],  
    \"font\": \"Helvetica\", 
    \"font_size\": 30,  
    \"font_color\": [0, 0, 0], 
    \"rotate\": 30, 
    \"opacity\": 0.1,  
    \"host\":\"https://fcimgwechat1.oss-cn-hangzhou.aliyuncs.com\",
    \"density\": [198.4251968503937, 283.46456692913387],
    \"url\":\"http://dev-wx.bazhongsq.cn\"
}")
        ]);
        $runtime = new RuntimeOptions([]);
        try {
            // 复制代码运行请自行打印 API 的返回值
            $res = $client->invokeFunctionWithOptions("fc-pdf", "pdf_add_watermark", $invokeFunctionRequest, $invokeFunctionHeaders, $runtime);
            echo "<pre>";
                 $databasesize = Utils::toString($res->body);

            var_dump($databasesize);
        }
        catch (Exception $error) {
            if (!($error instanceof TeaError)) {
                $error = new TeaError([], $error->getMessage(), $error->getCode(), $error);
            }
            var_dump($error->message);
            // 如有需要，请打印 error
            Utils::assertAsString($error->message);
        }
    }
}
 require './vendor/autoload.php';

Sample::main();
